epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES","10000000")
epicsEnvSet("P", "LabS:Ctrl")
epicsEnvSet("IOCNAME",   "LabS:Ctrl-IOC-042")
epicsEnvSet("IOCDIR",    "LabS_Ctrl-IOC-042")

require ipmimanager
require essioc

# when set to 0 console will not produce message after ioc init, when set to 1 all messages will be visible
epicsEnvSet("DISP_MSG", 0)

# crate IP address
epicsEnvSet("CRATE_NUM", "1")
epicsEnvSet("MCH_ADDR", "172.30.6.67")
epicsEnvSet("TELNET_PORT", "23")

# 1=enable/0=disable INTERNAL archiver
epicsEnvSet("ARCHIVER", "0")
# INTERNAL archiver size
epicsEnvSet("ARCHIVER_SIZE", 1024)

# Panels compatibility version
epicsEnvSet("PANEL_VER", "2.1.0")

# Special naming mode 0 -> standard naming convention 1 -> special mode
epicsEnvSet("NAME_MODE", 0)

# Deployment path (with substitutions files)
epicsEnvSet("DEPLOYMENT_DIR", "$(E3_CMD_TOP)")

###########################
# connect to specific MCH
############################
epicsEnvSet("MTCA_PREF", "$(P)-MTCA-$(CRATE_NUM)00:")
epicsEnvSet("IOC_PREF",  "$(P)-IOC-042:")
#
#
####################################################################
# Demonstration crate components
####################################################################
#  MTCA Power Supp   (fruId: 51, state: 4, sensors:  28, slot:  32)
#     uTCA Cooling   (fruId: 40, state: 4, sensors:  13, slot:  48)
#          SIS8900   (fruId: 94, state: 4, sensors:   4, slot:  21)
#     mTCA-EVR-300   (fruId:  5, state: 4, sensors:  13, slot:   1)
#       AM 900/412   (fruId:  6, state: 4, sensors:  11, slot:   2)
#       AM 900/412   (fruId:  7, state: 1, sensors:  11, slot:   3)
#        SIS8300KU   (fruId:  9, state: 4, sensors:  19, slot:   5)
#         IFC_1410   (fruId: 10, state: 4, sensors:  22, slot:   6)
#        MCH-Clock   (fruId: 60, state: 4, sensors:   6, slot:  64)
#         MCH-PCIe   (fruId: 61, state: 4, sensors:  11, slot:  65)
####################################################################
#
#

epicsEnvSet("SLOT1_MODULE", "EVR")
epicsEnvSet("SLOT1_IDX", "42")

epicsEnvSet("SLOT2_MODULE", "CPU")
epicsEnvSet("SLOT2_IDX", "42")

epicsEnvSet("SLOT3_MODULE", "CPU")
epicsEnvSet("SLOT3_IDX", "43")

#-epicsEnvSet("SLOT4_MODULE", "XXX")
#-epicsEnvSet("SLOT4_IDX", "NN")

epicsEnvSet("SLOT5_MODULE", "AMC")
epicsEnvSet("SLOT5_IDX", "42")

epicsEnvSet("SLOT6_MODULE", "AMC")
epicsEnvSet("SLOT6_IDX", "43")

epicsEnvSet("SLOT21_MODULE", "RTM")

epicsEnvSet("CHASSIS_CONFIG","SLOT1_MODULE=$(SLOT1_MODULE)", "SLOT1_IDX=$(SLOT1_IDX)", "SLOT2_MODULE=$(SLOT2_MODULE)", "SLOT2_IDX=$(SLOT2_IDX)", "SLOT3_MODULE=$(SLOT3_MODULE)","SLOT3_IDX=$(SLOT3_IDX)", "SLOT5_MODULE=$(SLOT5_MODULE)", "SLOT5_IDX=$(SLOT5_IDX)", "SLOT6_MODULE=$(SLOT6_MODULE)", "SLOT6_IDX=$(SLOT6_IDX)", "SLOT21_MODULE=$(SLOT21_MODULE)")

# load db templates for your hardware configuration
iocshLoad("chassis.iocsh")

iocshLoad("$(ipmimanager_DIR)connect.iocsh", "DB_NAME=dynamic, MCH_ADDR=$(MCH_ADDR), ARCHIVER=$(ARCHIVER), ARCHIVER_SIZE=$(ARCHIVER_SIZE), P=$(P), CRATE_NUM=$(CRATE_NUM), TIMEOUT=10, USE_STREAM=, STREAM_PORT=$(TELNET_PORT),CHECK_VER=#,PANEL_VER=$(PANEL_VER),NAME_MODE=$(NAME_MODE),MTCA_PREF=$(MTCA_PREF),IOC_PREF=$(IOC_PREF),USE_EXPERT=,$(CHASSIS_CONFIG),DEPLOYMENT_DIR=$(DEPLOYMENT_DIR)")  # With steam reading links (mch not password protected)

# Load essioc
iocshLoad("$(essioc_DIR)/common_config.iocsh")


iocInit()

eltc "$(DISP_MSG)"
